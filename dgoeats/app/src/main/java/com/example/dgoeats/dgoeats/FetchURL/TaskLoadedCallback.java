package com.example.dgoeats.dgoeats.FetchURL;

/**
 * Created by Vishal on 10/20/2018.
 */

public interface TaskLoadedCallback {
    void onTaskDone(Object... values);
}
