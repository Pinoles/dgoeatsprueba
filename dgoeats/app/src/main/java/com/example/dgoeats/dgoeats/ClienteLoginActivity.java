package com.example.dgoeats.dgoeats;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

public class ClienteLoginActivity extends AppCompatActivity {


    private EditText nEmail,nContrasena;
    private Button nIniciar, nRegistrar;
    private CheckBox nCheckBox;

    private FirebaseAuth nAuth;
    private FirebaseAuth.AuthStateListener fireBaseAuthListener;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cliente_login);
/*
        SharedPreferences preferences = getSharedPreferences("datos", Context.MODE_PRIVATE);
        nEmail.setText(preferences.getString("mail",""));
        nContrasena.setText(preferences.getString("contra",""));
*/
        nAuth = FirebaseAuth.getInstance();

        fireBaseAuthListener = new FirebaseAuth.AuthStateListener() {
            @Override
            public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
                FirebaseUser usuario = FirebaseAuth.getInstance().getCurrentUser();
                if(usuario !=null){
                    Intent Intent = new Intent(ClienteLoginActivity.this , Main2Activity.class);
                    startActivity(Intent);
                    finish();
                    return;
                }
            }
        };

        nEmail = (EditText) findViewById(R.id.email);
        nContrasena = (EditText) findViewById(R.id.contrasena);

        nIniciar = (Button) findViewById(R.id.iniciar);
        nRegistrar = (Button) findViewById(R.id.registrar);

        nRegistrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                final String email = nEmail.getText().toString();
                final String contrasena = nContrasena.getText().toString();
                nAuth.createUserWithEmailAndPassword(email,contrasena).addOnCompleteListener(ClienteLoginActivity.this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if(!task.isSuccessful()){
                            Toast.makeText(ClienteLoginActivity.this,"error en iniciar sesion",Toast.LENGTH_SHORT).show();
                        }else{

                            String id_usuario = nAuth.getCurrentUser().getUid();
                            DatabaseReference current_usuario_id = FirebaseDatabase.getInstance().getReference().child("Users").child("cliente").child(id_usuario);
                            current_usuario_id.setValue(true);

                        }
                    }
                });
            }
        });



        nIniciar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                final String email = nEmail.getText().toString();
                final String contrasena = nContrasena.getText().toString();

                nAuth.signInWithEmailAndPassword(email,contrasena).addOnCompleteListener(ClienteLoginActivity.this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if(!task.isSuccessful()){
                            Toast.makeText(ClienteLoginActivity.this,"Error en iniciar",Toast.LENGTH_SHORT);
                        }

                    }
                });
            }
        });

    }
    /*
    public void Guardar(){
        SharedPreferences preferences = getSharedPreferences("datos",Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString("mail",nEmail.getText().toString());
        editor.putString("contra",nContrasena.getText().toString());
        editor.commit();
    }*/

    @Override
    protected void onStart() {
        super.onStart();
        nAuth.addAuthStateListener(fireBaseAuthListener);
    }

    @Override
    protected void onStop() {
        super.onStop();
        nAuth.removeAuthStateListener(fireBaseAuthListener);
    }
}