package com.example.dgoeats.dgoeats;

import android.Manifest;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.os.Build;
import android.os.Bundle;
import android.os.Looper;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentActivity;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.directions.route.AbstractRouting;
import com.directions.route.Route;
import com.directions.route.RouteException;
import com.directions.route.Routing;
import com.directions.route.RoutingListener;
import com.firebase.geofire.GeoFire;
import com.firebase.geofire.GeoLocation;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public class ClienteMapaActivity extends FragmentActivity implements RoutingListener, GoogleApiClient.ConnectionCallbacks, OnMapReadyCallback {

    int a = 0;

   // private Location nLastLocation;

    private LatLng latLng;

    private GoogleMap mMap;

    LocationRequest nLocationRequest;

    private Button Confirmar,ok;

    private EditText mEditText;

    //private TextInputEditText mEditTextIn;

    //private String destination;

    private FusedLocationProviderClient mFusedLocationProviderClient;

    private static final int[] COLORS = new int[]{R.color.quantum_lightblueA700};

   // private GoogleApiClient mGoogleApiClient;

     LatLng start;
    protected LatLng end;

    private int status = 1;

    Marker mEnd,mStart;

    private Polyline currentPolyline;


    private List<Polyline> polylines;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cliente_mapa);
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.

        mFusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(this);

        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        start=new LatLng(0.0,0.0);

        status= 1;

        polylines = new ArrayList<>();

        Confirmar = (Button) findViewById(R.id.request);
        //ok = (Button) findViewById(R.id.Validar);

        mEditText = (EditText) findViewById(R.id.Lugar);

        //mEditTextIn = (TextInputEditText) findViewById(R.id.enter);
        double lat = getIntent().getDoubleExtra("Lat",0.0);
        double lng = getIntent().getDoubleExtra("Lng",0.0);
        end = new LatLng(lat , lng);
        if(end.latitude == 0.0 && end.longitude == 0.0 ){
            Toast.makeText(ClienteMapaActivity.this,"eo",Toast.LENGTH_SHORT);
        }

        Confirmar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {




                if(start.latitude!=0.0 && start.longitude!=0.0) {
                    if (status == 1) {
                        status = 2;

                        mEnd = mMap.addMarker(new MarkerOptions().position(end));

                        //mMap.addMarker(new MarkerOptions().position(end));

                        Routing routing = new Routing.Builder()
                                .travelMode(AbstractRouting.TravelMode.DRIVING)
                                .withListener(ClienteMapaActivity.this)
                                .alternativeRoutes(false)
                                .waypoints(end, start)
                                .key("AIzaSyAFsX0YWb0mwmm3ozKGieQPTMpc21SlUn4")
                                .build();

                        routing.execute();

                    } else {
                        status = 1;
                        erasePolylines();
                        mEnd.remove();
                        mStart.remove();
                    }
                }else{
                    Toast.makeText(ClienteMapaActivity.this,"Ingrese ubicacion",Toast.LENGTH_SHORT).show();
                }
            }
        });

/*        ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    start = getLatLngFromName();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                //mStart = mMap.addMarker(new MarkerOptions().position(start));
            }
        });*/

    }


    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;


        nLocationRequest = new LocationRequest();
        nLocationRequest.setInterval(1000);
        nLocationRequest.setFastestInterval(1000);
        nLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);

        mMap.moveCamera(CameraUpdateFactory.newLatLng(new LatLng(24.027597, -104.652924)));
        mMap.animateCamera(CameraUpdateFactory.zoomTo(14));

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
                mFusedLocationProviderClient.requestLocationUpdates(nLocationRequest, mLocationCallBack, Looper.myLooper());
                mMap.setMyLocationEnabled(true);
            } else {
                checkLocationPermission();
            }
        }

        mMap.setOnMapLongClickListener(new GoogleMap.OnMapLongClickListener() {
            @Override
            public void onMapLongClick(LatLng latLng) {
                //Reset marker when already 2
                if (mStart != null) {
                    mStart.remove();
                }
                //Save first point select

                //Create marker

                start = (latLng);

                mStart = mMap.addMarker(new MarkerOptions().position(start));

                mEditText.setText(getCityName(latLng));
                //    markerOptions.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_RED));



            }
        });

    }




    LocationCallback mLocationCallBack = new LocationCallback() {
        @Override
        public void onLocationResult(LocationResult locationResult) {
            for (Location location : locationResult.getLocations()) {
               /* if(getApplicationContext()!=null){
                    /*rideDistance += nLastLocation.distanceTo(Location)/1000;
                */


                //nLastLocation = location;




                latLng = new LatLng(location.getLatitude(), location.getLongitude());

                if(String.valueOf(mEditText.getText()).equals("")) {
                    mEditText.setText(getCityName(latLng));
                    mStart = mMap.addMarker(new MarkerOptions().position(latLng));
                    start = latLng;
                }

                //start = new LatLng(location.getLatitude(), location.getLongitude());

               /* mMap.moveCamera(CameraUpdateFactory.newLatLng(latLng));
                mMap.animateCamera(CameraUpdateFactory.zoomTo(15));
*/

                String userId = FirebaseAuth.getInstance().getCurrentUser().getUid();
                DatabaseReference ref = FirebaseDatabase.getInstance().getReference("ClienteUbicacion");

                GeoFire geoFire = new GeoFire(ref);

                geoFire.setLocation(userId, new GeoLocation(location.getLatitude(), location.getLongitude()));
            }
        }
    };

    private String getCityName(LatLng myCoordinates) {
        String myCity = "";
        Geocoder geocoder = new Geocoder(ClienteMapaActivity.this, Locale.getDefault());
        try {
            List<Address> addresses = geocoder.getFromLocation(myCoordinates.latitude, myCoordinates.longitude, 1);
            String address = addresses.get(0).getAddressLine(0);
            myCity = addresses.get(0).getAddressLine(0);
            Log.d("mylog", "Complete Address: " + addresses.toString());
            Log.d("mylog", "Address: " + address);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return myCity;
    }

    private LatLng getLatLngFromName() throws IOException {
        Geocoder gc = new Geocoder(ClienteMapaActivity.this);
//...
        List<Address> list = gc.getFromLocationName(mEditText.getText().toString(), 1);

        Address address = list.get(0);

        double lat = address.getLatitude();
        double lng = address.getLongitude();
        return new LatLng(lat,lng);
    }

    private void checkLocationPermission() {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.ACCESS_FINE_LOCATION)) {
                new AlertDialog.Builder(this)
                        .setTitle("Dar permiso")
                        .setMessage("dar PErmiso")
                        .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                ActivityCompat.requestPermissions(ClienteMapaActivity.this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, 1);
                            }
                        })
                        .create()
                        .show();

            } else {
                ActivityCompat.requestPermissions(ClienteMapaActivity.this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, 1);

            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case 1:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                        // TODO: Consider calling
                        //    ActivityCompat#requestPermissions
                        // here to request the missing permissions, and then overriding
                        //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                        //                                          int[] grantResults)
                        // to handle the case where the user grants the permission. See the documentation
                        // for ActivityCompat#requestPermissions for more details.
                        return;
                    }
                    mFusedLocationProviderClient.requestLocationUpdates(nLocationRequest, mLocationCallBack, Looper.myLooper());
                    mMap.setMyLocationEnabled(true);
                } else {
                    Toast.makeText(getApplicationContext(), "Permiso porfavor", Toast.LENGTH_SHORT).show();
                }
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (mFusedLocationProviderClient != null) {
            mFusedLocationProviderClient.removeLocationUpdates(mLocationCallBack);
        }
        String userId = FirebaseAuth.getInstance().getCurrentUser().getUid();
        DatabaseReference ref = FirebaseDatabase.getInstance().getReference("ClienteUbicacion");

        GeoFire geoFire = new GeoFire(ref);
        geoFire.removeLocation(userId);
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {

    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onRoutingFailure(RouteException e) {
        if (e != null) {
            Toast.makeText(this, "Error: " + e.getMessage(), Toast.LENGTH_LONG).show();
        } else {
            Toast.makeText(this, "Something went wrong, Try again", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onRoutingStart() {

    }


    @Override
    public void onRoutingSuccess(ArrayList<Route> arrayList, int i) {
        if (polylines.size() > 0) {
            for (Polyline poly : polylines) {
                poly.remove();
            }
        }

        polylines = new ArrayList<>();
        //add route(s) to the map.
        for (int a = 0; a < arrayList.size(); a++) {

            //In case of more than 5 alternative routes
            int colorIndex = a % COLORS.length;

            PolylineOptions polyOptions = new PolylineOptions();
            polyOptions.color(getResources().getColor(COLORS[colorIndex]));
            polyOptions.width(10 + a * 3);
            polyOptions.addAll(arrayList.get(a).getPoints());
            Polyline polyline = mMap.addPolyline(polyOptions);
            polylines.add(polyline);

            Toast.makeText(getApplicationContext(), "Route " + (a + 1) + ": distance - " + arrayList.get(a).getDistanceValue() + ": duration - " + arrayList.get(a).getDurationValue(), Toast.LENGTH_SHORT).show();
        }

    }

    @Override
    public void onRoutingCancelled() {

    }

    private void erasePolylines() {
        for (Polyline line : polylines) {
            line.remove();
        }
        polylines.clear();
    }
}

 /*Places.initialize(getApplicationContext(), "AIzaSyAFsX0YWb0mwmm3ozKGieQPTMpc21SlUn4");

        if (!Places.isInitialized()) {
            Places.initialize(getApplicationContext(), "AIzaSyAFsX0YWb0mwmm3ozKGieQPTMpc21SlUn4");
        }

        AutocompleteSupportFragment autocompleteFragment = (AutocompleteSupportFragment)
                getSupportFragmentManager().findFragmentById(R.id.autocomplete_fragment);

        autocompleteFragment.setPlaceFields(Arrays.asList(Place.Field.ID, Place.Field.NAME,Place.Field.LAT_LNG));

        autocompleteFragment.setOnPlaceSelectedListener(new PlaceSelectionListener() {
            @Override
            public void onPlaceSelected(Place place) {
                // TODO: Get info about the selected place.
                if(place.getLatLng() == null){Toast.makeText(ClienteMapaActivity.this,"Error en posicion",Toast.LENGTH_SHORT).show();}
                else{start = place.getLatLng(); mMap.addMarker(new MarkerOptions().position(place.getLatLng())); }
            }

            @Override
            public void onError(Status status) {
                // TODO: Handle the error.
            }
        });

*/

     /*
    private String getUrl(LatLng origin, LatLng dest, String directionMode) {
        // Origin of route
        String str_origin = "origin=" + origin.latitude + "," + origin.longitude;
        // Destination of route
        String str_dest = "destination=" + dest.latitude + "," + dest.longitude;
        // Mode
        String mode = "mode=" + directionMode;
        // Building the parameters to the web service
        String parameters = str_origin + "&" + str_dest + "&" + mode;
        // Output format
        String output = "json";
        // Building the url to the web service
        String url = "https://maps.googleapis.com/maps/api/directions/" + output + "&" + parameters + "&key=AIzaSyARxomwngnA9X08iUkKVGoD4Uvfjz-mKTk";
        return url;
    }

    @Override
    public void onTaskDone(Object... values) {
        if (currentPolyline != null)
            currentPolyline.remove();
        currentPolyline = mMap.addPolyline((PolylineOptions) values[0]);
    }
*/

     /*if (listPoints.size() == 2) {
                    //Create the URL to get request from first marker to second marker
                    String url = getRequestUrl(listPoints.get(0), listPoints.get(1));
                    TaskRequestDirections taskRequestDirections = new TaskRequestDirections();
                    taskRequestDirections.execute(url);
                }*/

     /*<android.support.design.widget.TextInputLayout
                android:layout_width="match_parent"
                android:layout_height="match_parent">

                <android.support.design.widget.TextInputEditText
                    android:layout_width="match_parent"
                    android:layout_height="wrap_content"
                    android:hint="hint"
                    android:id="@+id/enter"/>
            </android.support.design.widget.TextInputLayout>*/
