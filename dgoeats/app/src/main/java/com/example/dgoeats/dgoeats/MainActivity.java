package com.example.dgoeats.dgoeats;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity {

    private Button nConductor, nCliente, nRestaurantes;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        nConductor = (Button) findViewById(R.id.conductor);
        nCliente = (Button) findViewById(R.id.clientes);
        nRestaurantes = (Button) findViewById(R.id.restaurantes);

        nConductor.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent Intent = new Intent(MainActivity.this, ConductorLoginActivity.class);
                startActivity(Intent);
                /*]finish();
                return;*/
            }
        });

        nCliente.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent Intent = new Intent(MainActivity.this, ClienteLoginActivity.class);
                startActivity(Intent);
                /*finish();
                return;*/
            }
        });

        nRestaurantes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent Intent = new Intent(MainActivity.this, ClienteRestaurantesActivity.class);
                startActivity(Intent);
                finish();
                return;
            }
        });
    }
}
