package com.example.dgoeats.dgoeats;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;

public class Main2Activity extends AppCompatActivity {

    Button btnEquipales, btnGorditas,btnSushi;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);

        btnEquipales = (Button) findViewById(R.id.Equipales);
        btnGorditas = (Button) findViewById(R.id.Gordas);
        btnSushi = (Button) findViewById(R.id.Sushi);

        btnEquipales.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent I = new Intent(Main2Activity.this, ClienteMapaActivity.class);
                I.putExtra("Lat",24.019705);
                I.putExtra("Lng",-104.690259);

                startActivity(I);
            }
        });

        btnGorditas.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent I = new Intent(Main2Activity.this, ClienteMapaActivity.class);
                I.putExtra("Lat",24.030109);
                I.putExtra("Lng",-104.653608);

                startActivity(I);
            }
        });

        btnSushi.setOnClickListener(new View.OnClickListener() {
             @Override
             public void onClick(View v) {
                 Intent I = new Intent(Main2Activity.this, ClienteMapaActivity.class);
                 I.putExtra("Lat",24.031130);
                 I.putExtra("Lng",-104.641669);

                 startActivity(I);
             }
         });


    }
}
