package com.example.dgoeats.dgoeats;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class ClienteRestaurantesActivity extends AppCompatActivity {

    private Button rEquipales;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cliente_restaurantes);

        rEquipales= (Button) findViewById(R.id.rEquipales);

        rEquipales.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent Intent = new Intent (ClienteRestaurantesActivity.this, RestuauranteActivity.class );
                startActivity(Intent);
                finish();
                return;
            }
        });
    }


}
